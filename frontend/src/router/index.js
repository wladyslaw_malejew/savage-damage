import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '@/components/Main.vue'
import Article from '@/components/Article.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Main,
  },
  {
    path: '/article/:title',
    component: Article
  }
]

const router = new VueRouter({
  mode: 'history',
  // hash: false,
  base: process.env.BASE_URL,
  routes
})

export default router
