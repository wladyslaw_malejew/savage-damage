'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {

	lifecycles: {
		async afterCreate(result) {
			console.log('trying to send mails');

			let usersData, userName, mailList;

			usersData = await strapi.plugins['users-permissions'].services.user.fetchAll()

			mailList = usersData.map(ud => ud.email)

			console.log(mailList);

			await strapi.plugins['email'].services.email.send({
			  to: mailList,
			  from: 'vm23lm10@gmail.com',
			  replyTo: 'vm23lm10@gmail.com',
			  subject: `${result.subject}`,
			  text: `${result.text}`,
			});

			console.log("success!!!")
		}
	}

// 	afterSave: async (model, response, options) => {
// 		console.log('trying to send mails');

// 		let usersData, userName, mailList;

//     usersData = await strapi.plugins['users-permissions'].services.user.fetchAll()

//     mailList = usersData.map(ud => ud.email)
//     // userName = usersData.map(ud => ud.username)

//     console.log(mailList);
    
//     await strapi.plugins['email'].services.email.send({
//       to: mailList,
//       // from: 'rockband@mail.com',
//       subject: `${response.subject}`,
//       text: `${response.text}`,
//     });

//     console.log("success!!!")
//   },
};
