module.exports = {
  apps: [
    {
      name: 'strapi',
      cwd: 'projects/savage-damage/backend',
      script: 'npm',
      args: 'start',
      env: {
        NODE_ENV: 'production',
        DATABASE_HOST: '127.0.0.1', // database endpoint
        DATABASE_PORT: '27017',
        DATABASE_NAME: 'savage-damage-db', // DB name
        DATABASE_USERNAME: 'strapi', // your username for psql
        DATABASE_PASSWORD: 'strapi', // your password for psql
      },
    },
  ],
};
